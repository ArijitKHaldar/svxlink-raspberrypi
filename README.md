## This is [SvxLink](https://github.com/sm0svx/svxlink), the Echolink Server for Linux


#### Who needs this software
- [x] You are on Linux operating system
- [x] You need a Sysop Echolink node to run in a Headless environment
- [ ] You need to use Echolink on GUI : Use Qtel instead
- [x] Setting up a Raspberry Pi Echolink linked-node or repeater

#### Before we begin

1. Get you Raspberry Pi or your preferred installation platform up and running
1. Get your installation platform connected to your router via WiFi or using LAN
1. Open a terminal on your installation platform and type `ip -c a`. If you connected via WiFi, `wlan0` or `wlp2s0` or anything named similar will have "UP" written for it after state tag.
    ![IP Listing](./Screenshots/IP.png)

    If you connected via LAN cable, `eth0` or `enp1s0f1` or anything named similar will have "UP" written for it after state tag
1. Now notice the IP address for that network adapter written after the `inet` tag; and the MAC address after the `link/ether` tag for that interface.
1. Login on your router configuration page, and bind the IP address and MAC address together, which we obtained in previous step.
    ![IP Binding](./Screenshots/IP_Binding.png "Make Raspberry Pi always get same IP address from router")
1. Next, on your router configuration page, go to port forwarding or virtual servers, and forward UDP ports 5198 and 5199 to your IP address that we noted.
    ![Port Forwarding](./Screenshots/Port_Forward.png "Forwarded UDP ports on D-Link DIR-615 Router")
1. An additional step may be required to allow these ports on your linux installation, if you are using a firewall on it. The ports we need are: 5198/UDP, 5199/UDP & 5200/TCP.

    Assuming that you use systemd firewalld, the configuration will be:-
    ```sh
    sudo firewall-cmd --permanent --zone=home --add-interface=wlan0
    sudo firewall-cmd --permanent --zone=home --add-port=5198/udp
    sudo firewall-cmd --permanent --zone=home --add-port=5199/udp
    sudo firewall-cmd --permanent --zone=home --add-port=5200/tcp
    ```
    Change the zone name and interface name as per your requirements and reboot your system.

Assuming that your network supports port forwarding, and these configurations that you have set works, then your are now ready to setup Svxlink.

_**[Note: Only one IP address can be set to port forward on your router. Do not keep multiple IPs forwarding same ports, and do not start Echolink without Proxy on your network on any other device.**_

_**If you have never tried to setup port forwarding, it is advisable to use a Windows system, and setup port forwarding for that device. Then check the Incoming and Outgoing firewall tests to check if your network works fine.**_

_**Once all tests succeed for the Windows system, setup Public Proxy for Echolink (Tools->Setup->Proxy) on the Windows system and change the port forwarding IP on your router from the Windows device to the Linux device's IP]**_

---
From now on, we are assuming that you are installing an Echolink Linked node on a Raspberry Pi 3B+ or 4 with atleast 1GB RAM. For any other, just check the installation steps required, then follow along after installation is done.

## INSTALLATION

1. Get everything up-to-date and reboot
    ```sh
    sudo apt update
    sudo apt full-upgrade -y
    sudo apt autoremove -y
    sudo apt clean
    sudo reboot
    ```
1. Install svxlink from Raspbian repository
    ```sh
    sudo apt install svxlink-server
    sudo usermod -a -G audio,plugdev,gpio,dialout svxlink
    ```
    [OR]
    
    Compile svxlink from source
    ```sh
    sudo apt install g++ cmake make libsigc++-2.0-dev libgsm1-dev libpopt-dev tcl-dev libgcrypt20-dev libspeex-dev libasound2-dev libopus-dev librtlsdr-dev doxygen groff alsa-utils vorbis-tools curl libcurl4-openssl-dev git rtl-sdr libcurl4-openssl-dev cmake libjsoncpp-dev
    sudo useradd -r -G audio,plugdev,gpio,dialout svxlink
    
    cd
    git clone https://github.com/sm0svx/svxlink.git
    mkdir svxlink/src/build
    cd svxlink/src/build
    cmake -DUSE_QT=OFF -DCMAKE_INSTALL_PREFIX=/usr -DSYSCONF_INSTALL_DIR=/etc -DLOCAL_STATE_DIR=/var -DWITH_SYSTEMD=ON ..
    make -j2
    make doc
    sudo make install
    sudo ldconfig
    ```
1. Install anouncement audio files
    ```sh
    cd /usr/share/svxlink/sounds/
    sudo wget https://github.com/sm0svx/svxlink-sounds-en_US-heather/releases/download/19.09/svxlink-sounds-en_US-heather-16k-19.09.tar.bz2
    sudo tar xvjf svxlink-sounds-en_US-heather-16k-19.09.tar.bz2
    sudo ln -s en_US-heather-16k en_US
    ```
## Setup USB Soundcard

Now our hardwares need to be connected with the Raspberry Pi. If you are using a USB to Serial adapter for PTT/SQL, then connect both your USB Soundcard and your USB to Serial Adapter, else connect only the USB Soundcard. It might be better to reboot the Raspberry Pi after plugging in new hardwares.

1. Find USB Soundcard
    ```sh
    arecord -l
    ```
    You should get a result similar to:
    >card 1: Device [USB Audio Device], device 0: USB Audio [USB Audio]
    >
    >Subdevices: 1/1
    >
    >Subdevice #0: subdevice #0
1. Edit AdvancedLinuxSoundArchitechture configuration
   
   Note the `card 1`, so our card number is 1. Edit accordingly
   ```sh
   sudo cp /usr/share/alsa/alsa.conf /usr/share/alsa/alsa.conf.bak
   sudo vim /usr/share/alsa/alsa.conf
   ```
   Edit the lines **default.ctl.card** and **default.pcm.card**. In my case I changed the number at the end of the line from “0” to “1” as arecord listed my sound card as Card 1, Device 0.

1. Save file and reboot Raspberry Pi for changes to take effect.

## Configure SvxLink

All files that are being changed are provided in this repository for reference while editing

1. Take backups of each file before changing

    ```sh
    sudo cp /etc/svxlink/svxlink.conf /etc/svxlink/svxlink.conf.bak
    sudo cp /etc/svxlink/gpio.conf /etc/svxlink/gpio.conf.bak
    sudo cp /etc/svxlink/svxlink.d/ModuleEcholink.conf /etc/svxlink/svxlink.d/ModuleEcholink.conf.bak
    sudo cp /lib/systemd/system/svxlink.service /etc/svxlink/svxlink.service.bak
    ```
1. Edit main Svxlink config file

    ```sh
    sudo vim /etc/svxlink/svxlink.conf
    ```
1. Edit Echolink login details

    ```sh
    sudo vim /etc/svxlink/svxlink.d/ModuleEcholink.conf
    ```
1. Edit required GPIO pins

    ```sh
    sudo vim /etc/svxlink/gpio.conf
    ```

## Configure autostart services

1. Stop running instance
    ```sh
    sudo systemctl stop svxlink
    ```
1. Add a delay to let Raspberry Pi connect to internet before starting SvxLink

    ```sh
    sudo vim /lib/systemd/system/svxlink.service
    ```
1. Restart all services
    ```sh
    sudo systemctl enable svxlink_gpio_setup.service
    sudo systemctl enable svxlink.service
    ```
1. Reboot SvxLink

## Test Running instance log

```sh
tail -f /var/log/svxlink
```

## Prevent overuse of SD Card by SvxLink log file

Edit `/etc/fstab/` file with sudo priviledge and add these lines at the end of the file. DO NOT DELETE ANYTHING ALREADY THERE

```plaintext
#temp filesystems for all the logging and stuff to keep it out of the SD card
tmpfs    /tmp    tmpfs    defaults,noatime,nosuid,size=100m    0 0
tmpfs    /var/tmp    tmpfs    defaults,noatime,nosuid,size=30m    0 0
tmpfs    /var/log    tmpfs    defaults,noatime,nosuid,mode=0755,size=100m    0 0
tmpfs    /var/spool/svxlink    tmpfs    defaults,noatime,nosuid,size=100m    0 0
```
